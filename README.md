Soap-UI Projekt enthält Beispiel HTPP-GET Queries, um räumliche Filter zu setzen.


Inhalt

Zwei WFS Diesnte werden abgefragt mit jeweils 3 Feature Types.:

HH_WFS_Drohnenflugverbotszonen
    - Drohnenflugverbotszonen Flugplätze (Fläche)
    - Hubschrauberlandeplätze (Punkt)
    - Drohnenflugverbotszonen Krankenhäuser (Fläche)

HH_WFS_hinderniserfassungsbereich_sonderlandeplatz
    Begrenzungslinien (Fläche)
    Anfluggrundlinie (Linie)
    Bemassungspfeile (Linie)

Bei der Ausführung der Queries wird gepfüft, ob ein Objekt in dem Suchumkreis gefunden wird, ausgehend von einem Ausgangspunkt.

Bei den Flächen wird mit einem 0m Radius geprüft und sind standartmäßig mit Punkten befüllt, die innerhalb der Verbotszone liegen.

Bei Punkten wird ein Umkreis von 1500m gesetzt und geprüft.


WFS-Filter

Ausgangspunkt kann unter dem Tag <gml:pos> geändert werden

Suchradius kann unter dem Tag <fes:Distance> geändert werden

Aufbau des Filters:

<fes:Filter>
    <fes:DWithin>
        <fes:ValueReference>geom</fes:ValueReference>
            <gml:Point gml:id="p1" srsName="EPSG:25832">
                <gml:pos>567146.9 5941402.2</gml:pos>
            </gml:Point>
        <fes:Distance uom='m'>0</fes:Distance>
    </fes:DWithin>
</fes:Filter>